function rcnn_model = rcnn_load_model(rcnn_model_or_file, mat_conv_net_cnn_file)
% rcnn_model = rcnn_load_model(rcnn_model_or_file, use_gpu)
%   Takes an rcnn_model structure and loads the associated Caffe
%   CNN into memory. Since this is nasty global state that is carried
%   around, a randomly generated 'key' (or handle) is returned.
%   Before making calls to caffe it's a good idea to check that
%   rcnn_model.cnn.key is the same as caffe('get_init_key').

% AUTORIGHTS
% ---------------------------------------------------------
% Copyright (c) 2014, Ross Girshick
%
% This file is part of the R-CNN code and is available
% under the terms of the Simplified BSD License provided in
% LICENSE. Please retain this notice and LICENSE if you use
% this file (or any portion of it) in your project.
% ---------------------------------------------------------
disp(rcnn_model_or_file);

rcnn_model_or_file.mat_conv_net_model = load(mat_conv_net_cnn_file);
rcnn_model_or_file.mat_conv_net_model = vl_simplenn_move(rcnn_model_or_file.mat_conv_net_model,'gpu');
rcnn_model = rcnn_model_or_file;


%{
1.Sets GPU mode and lots of other stuff to Configure Caffe
2.Probably we won't need all this
%}
%vl_compilenn('enableGpu', true, 'cudaMethod', 'nvcc', 'cudaRoot', '/usr/local/cuda-7.0/',
%'enableCudnn', true, 'cudnnRoot', '/home/abhinav/matconvnet-1.0-beta16/cudnn');
