function feat = rcnn_features(im, boxes, rcnn_model)
disp('Calculating Features for image');
% feat = rcnn_features(im, boxes, rcnn_model)
%   Compute CNN features on a set of boxes.
%
%   im is an image in RGB order as returned by imread
%   boxes are in [x1 y1 x2 y2] format with one box per row
%   rcnn_model specifies the CNN Caffe net file to use.

% AUTORIGHTS
% ---------------------------------------------------------
% Copyright (c) 2014, Ross Girshick
%
% This file is part of the R-CNN code and is available
% under the terms of the Simplified BSD License provided in
% LICENSE. Please retain this notice and LICENSE if you use
% this file (or any portion of it) in your project.
% ---------------------------------------------------------

% make sure that caffe has been initialized for this model
%{NOTE: No Caffe  no init keys%}

% Each batch contains 256 (default) image regions.
% Processing more than this many at once takes too much memory
% for a typical high-end GPU.
[batches, batch_padding] = rcnn_extract_regions(im, boxes, rcnn_model);
batch_size = rcnn_model.cnn.batch_size;

% compute features for each batch of region images
feat_dim = -1;
feat = [];
curr = 1;
% res(end-1).x is giving 1*4096 Matrix as an output
no_of_feats_per_region = 4096;
feat = zeros( no_of_feats_per_region , length(batches)*batch_size - batch_padding);
F = gpuArray(feat);
disp(size(feat));
region_operated=1;

for j = 1:length(batches)
  % forward propagate batch of region images
  %{NOTE: Like Caffe MatconvNet cannot process a batch of images, so we will Processing
  %image by image in MatconvNet
  %}
  %f = caffe('forward', batches(j));
  %f = f{1};
  %f = f(:);
  disp('Batch No');
  disp(j);
  cur_batch = batches{j};
  G = gpuArray(cur_batch);
  for batch_no = 1:batch_size
      if j == length(batches) && batch_padding > 0 && batch_size - batch_no <= batch_padding
        break
      else
        % disp('Image Size');
        % disp(size(cur_batch(:,:,:,batch_no)));

        res = vl_simplenn( rcnn_model.mat_conv_net_model, G(:,:,:,batch_no));
        res_lay = res(end-2).x;
        res_lay = reshape(res_lay, [no_of_feats_per_region 1]);
        % feat(:,region_operated) = zeros(no_of_feats_per_region,1);
        % disp('res_lay');
        % disp(size(res_lay));
        % disp('feat');
        % disp(size(feat(:,region_operated)));
        % disp('region_operated');
        % disp(region_operated);
        F(:,region_operated)  = res_lay;
        region_operated = region_operated + 1;
      end

  end
end
feat = gather(F);
feat = feat';
