function rcnn_model = rcnn_create_model( mat_conv_net_cnn_file, cache_name)
%{Initial Input Parameters cnn_definition_file, cnn_binary_file, cache_name, mat_conv_net_cnn_file%}
% AUTORIGHTS
% ---------------------------------------------------------
% Copyright (c) 2014, Ross Girshick
%
% This file is part of the R-CNN code and is available
% under the terms of the Simplified BSD License provided in
% LICENSE. Please retain this notice and LICENSE if you use
% this file (or any portion of it) in your project.
% ---------------------------------------------------------
%{NOTE:cnn_definition_file is not required for MatconvNEt
%}
disp(mat_conv_net_cnn_file);

%if ~exist('cache_name', 'var') || isempty(cache_name)
%  cache_name = 'none';
%end

%  model =
%    cnn: [1x1 struct]
%        binary_file: 'path/to/cnn/model/binary'
%        definition_file: 'path/to/cnn/model/definition'
%        batch_size: 256
%        image_mean: [227x227x3 single]
%        init_key: -1
%    detectors.W: [N x <numclasses> single]  % matrix of SVM weights
%    detectors.B: [1 x <numclasses> single]  % (row) vector of SVM biases
%    detectors.crop_mode: 'warp' or 'square'
%    detectors.crop_padding: 16
%    detectors.nms_thresholds: [1x20 single]
%    training_opts: [1x1 struct]
%        bias_mult: 10
%        fine_tuned: 1
%        layer: 'fc7'
%        pos_loss_weight: 2
%        svm_C: 1.0000e-03
%        trainset: 'trainval'
%        use_flipped: 0
%        year: '2007'
%        feat_norm_mean: 20.1401
%    classes: {cell array of class names}
%    class_to_index: map from class name to column index in W
%{
NOTE: This Just Initializes a Structure Without Actually loading
the model.

This Function Does three Things Primarily -
1.Initializes Batch Size (Don't Know ) and Input Size and Init Key ( Don't know )

2.Loads the Data Mean File as we are operating on the same dataset, the mean won't change

3.Initializes room for the SVM detector which will be trained and added later

4.
%}
%Initial Value 256
cnn.batch_size = 500;
cnn.init_key = -1;

%{
NOTE: Image input_size of VGG 16 224 * 224 * 3
%}

cnn.input_size = 224;
% load the ilsvrc image mean need be changed as mean is same for our set also

data_mean_file = './external/caffe/matlab/caffe/ilsvrc_2012_mean.mat';
assert(exist(data_mean_file, 'file') ~= 0);
ld = load(data_mean_file);
image_mean = ld.image_mean; clear ld;
off = floor((size(image_mean,1) - cnn.input_size)/2)+1;
image_mean = image_mean(off:off+cnn.input_size-1, off:off+cnn.input_size-1, :);
%This Image mean is important and used irrespecive of library

%{NOTE:Image Mean is modified as per input image size requirement
%}
cnn.image_mean = image_mean;

%{
NOTE: These Detectors are SVMS in the future
%}
% init empty detectors
detectors.W = [];
detectors.B = [];
detectors.crop_mode = 'warp';
detectors.crop_padding = 16;
detectors.nms_thresholds = [];

% rcnn model wraps the convnet and detectors
rcnn_model.cnn = cnn;
rcnn_model.cache_name = cache_name;
rcnn_model.detectors = detectors;
