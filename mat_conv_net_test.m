net = load('~/Datasets/pre_trained_networks/imagenet-vgg-verydeep-16.mat');
im = imread('peppers.png');
im = im2single(im);
im = imresize(im, [224 224]);
res = vl_simplenn(net, im);
res_x = reshape ( res(end-1).x , [1000 1] );
feat = zeros(1000,2);
